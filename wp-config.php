<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wdl');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'j-@|b=:Sn|>|$@vtj|91+{J:3b+(v!n8N6Es(qOpR0b|f5rM!_Ib<B8y:4RPOoJV');
define('SECURE_AUTH_KEY',  'vrN8:Kf.qs-r@0cFU`G:A&<>n5,3KzSp@~^ WQ>|3zqz6S{CBpQ.lr8h$nWN&Tro');
define('LOGGED_IN_KEY',    'SW_iD9s#k`[s#b2T7Hs8&lPmEAdtbpb]wqylba6pY_gkq9?QI4drw$hP4d&RIzvw');
define('NONCE_KEY',        'cKS+IKX:Na 7LQY)@cq}rv3@}~29TIzer<@N?1`.+ufJoy*q)1H+ZsXK4;xW(R.K');
define('AUTH_SALT',        'Xis%zrrj8QEHNJ](w/]I!1Bz,h-T3acmI1c(i~ dyn]uu~yJ_}u2IoqN<)lnHbnG');
define('SECURE_AUTH_SALT', 'U^>@uSIW+5fF(-VpKz2lwO#Z:)(6W2..3~;mI..Y8>2Y$WK:E2+mlP:7d,?{ve}b');
define('LOGGED_IN_SALT',   'mM_,jt&X}_.G-|LCs-;^!:2vgZB|ag<F3G:qtXQP>SK3&lv(2H0!Eu~SfJ*b{ll0');
define('NONCE_SALT',       '1w z+lD-Z#8#y@)2C$7H$6[5qukEf bJ]>9%Wo_N%:fh:dCj1g})MO?>C&Cg2}l]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
